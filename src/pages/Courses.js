// import {Fragment} from 'react';
import coursesData from './../mockData/courses';
import CourseCard from './../components/CourseCard';
import {useContext} from 'react';
import UserContext from './../UserContext';

export default function Course(){
	// console.log(coursesData[0])

	const {user, setUser} = useContext(UserContext);
	console.log(user);

	const courses = coursesData.map(course => {
		// console.log(course)
		return <CourseCard key={course.id} courseProp={course}/>
	})
	
	return(
		// {/*<Fragment>{courses}</Fragment>*/}
		courses
	)
}