import {Fragment} from 'react';
import Banner from './../components/Banner';
import CourseCard from './../components/CourseCard';
import Highlights from './../components/Highlights';

export default function Home(){

	const data = {
		title: "Welcome to Course Booking",
		description: "Opportunities for everyone, everywhere",
		destination: "/courses",
		buttonDesc: "Check the courses"
	}

	return(
		//render navbar, banner & footer in the webpage via home
		<Fragment>
			<Banner bannerProp={data}/>
			<Highlights/>
		</Fragment>
	)
}