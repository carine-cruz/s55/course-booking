import {Form, Button, Row, Col, Container} from 'react-bootstrap';
import {useState, useEffect} from 'react';
import {useNavigate} from 'react-router-dom';



export default function Login(){

	//React hooks ------------------------------
	const [email, setEmail] = useState("");
	const [pw, setPw] = useState("");
	const [isDisabled, setIsDisabled] = useState(true);

	const navigate = useNavigate();

	//UseEffect hook ----------------------------
	useEffect(() => {
		if(email!=="" && pw!==""){
			setIsDisabled(false);
		} else {
			setIsDisabled(true);
		}
	}, [email,pw])


	//Event listener -----------------------------
	const authenticate = (e) => {
		e.preventDefault();
		
		fetch('https://sleepy-gorge-55333.herokuapp.com/api/users/login', {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: pw
			})
		})
		.then(result => result.json())
		.then(result => {
			
			if(result.token){
				localStorage.setItem('token', result.token);

				localStorage.setItem('email', email)
				alert('You are now logged in.');

				//restore state to default
				setEmail("");
				setPw("");

				navigate('/courses');

				//console.log(localStorage.getItem('email')) //null if not existing
				
			} else {
				alert(`Username/password do not match.`)
				
			}

		});

	}
	// console.log(email);
	// console.log(pw)
	//content ------------------------------------
	return (
		<Container className="m-5">
			<h3 className="text-center">Log-in</h3>
			<Row className="justify-content-center">
				<Col xs={12} md={6}>
					<Form onSubmit={(e) => authenticate(e)}>

						<Form.Group className="mb-3">
					  	    <Form.Label>Email Address:</Form.Label>
					  	    <Form.Control type="email" value={email} onChange={(e)=> setEmail(e.target.value)}/>
					  	</Form.Group>

					  	<Form.Group className="mb-3">
					  		<Form.Label>Password</Form.Label>
					    	<Form.Control type="password" placeholder="Password" value={pw} onChange={(e)=> setPw(e.target.value)}/>
					  	</Form.Group>

					  	<Form.Group className="mb-3">
					  		<Form.Check type="checkbox" label="Check me out" />
					  	</Form.Group>
					  
					  <Button 
					  	variant="info" 
					  	type="submit"
					  	disabled={isDisabled}
					  >
					    Submit
					  </Button>
					</Form>
				</Col>
			</Row>
		</Container>
	)
}