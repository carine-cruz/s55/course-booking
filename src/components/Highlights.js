import {Card, Row, Col, Button} from "react-bootstrap";

export default function Highlights(){
	return(
		<Row className="m-5">
			<Col xs={12} md={4}>
				<Card>
				  <Card.Body>
				    <Card.Title>Learn From Home</Card.Title>
				    <Card.Text>
				    	Lorem ipsum dolor, sit, amet consectetur adipisicing elit. Molestias voluptatem nihil at reiciendis eos dicta reprehenderit repellendus rerum exercitationem? Repudiandae deserunt culpa voluptates et animi sit sapiente, adipisci vitae libero.
				    </Card.Text>
				  </Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card>
				  <Card.Body>
				    <Card.Title>Study Now Pay Later</Card.Title>
				    <Card.Text>
				    	Lorem ipsum dolor, sit, amet consectetur adipisicing elit. Molestias voluptatem nihil at reiciendis eos dicta reprehenderit repellendus rerum exercitationem? Repudiandae deserunt culpa voluptates et animi sit sapiente, adipisci vitae libero.
				    </Card.Text>
				  </Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card>
				  <Card.Body>
				    <Card.Title>Be Part of Our Community</Card.Title>
				    <Card.Text>
				    	Lorem ipsum dolor, sit, amet consectetur adipisicing elit. Molestias voluptatem nihil at reiciendis eos dicta reprehenderit repellendus rerum exercitationem? Repudiandae deserunt culpa voluptates et animi sit sapiente, adipisci vitae libero.
				    </Card.Text>
				  </Card.Body>
				</Card>
			</Col>
		</Row>
	)
}