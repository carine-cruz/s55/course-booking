import {Card, Button} from 'react-bootstrap';
//import user hook
import {useState, useEffect} from 'react';

export default function CourseCard({courseProp}){
	
	// console.log(props); 

	/*
	{courseProp: 
		{
			id: "", 
			name: "", 
			description: "", 
			price: "", 
			onOffer: ""
		}
	}*/
	
	//destructure array
	let [count, setCount] = useState(0);
	let [seats, setSeats] = useState(30);
	let [students, setStudents] = useState(0);

	const {name, description, price} = courseProp;

	useEffect(()=>{
		console.log('render');

	},[count])

	// console.log({courseProp})
	/*
		{
			id: "", 
			name: "", 
			description: "", 
			price: "", 
			onOffer: ""
		}
	*/

	// console.log(name);
	// console.log(description);
	// console.log(price);

	//function to handle button event
	const handleClick = () => {
		// console.log(`I'm clicked`, count + 1);
		// count++;
		setCount(count+1)
		if (seats == 0){
			alert('No more seats');

		}else {
			setSeats(seats-1);
			setStudents(students+1);
		}
	}

	return(
		<Card className="mx-2 my-5">
		  
		  <Card.Body>
		    <Card.Title>{name}</Card.Title>
		    <Card.Subtitle>Description</Card.Subtitle>
		    <Card.Text>
			     {description}
		    </Card.Text>
		    <Card.Subtitle>
		    	Price:
		   	</Card.Subtitle>
		   	<Card.Text>
		    	  {price}
		    </Card.Text>
		    <Card.Subtitle>Enrollees</Card.Subtitle>
		    <Card.Text>{students}</Card.Text>
		    <Card.Text>Count: {count}</Card.Text>
		    <Button className=" btn-info" onClick={handleClick}>Enroll</Button>
		  </Card.Body>
		</Card>
	)
}