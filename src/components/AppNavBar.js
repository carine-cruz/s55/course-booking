import {Navbar, Container, Nav, Form, FormControl, Button} from 'react-bootstrap'
import {Fragment} from 'react';

export default function AppNavBar(){

	const email = localStorage.getItem('email');
	//console.log(email)

	return(
		<Navbar bg="info" expand="lg">
		  <Navbar.Toggle aria-controls="basic-navbar-nav" />
		  <Navbar.Collapse id="basic-navbar-nav">
		    <Nav className="mr-auto">
		      	<Nav.Link href="/" className="text-light">Home</Nav.Link>
		      	<Nav.Link href="/courses" className="text-light">Courses</Nav.Link>
		       	{
		       		email !== null 
		       		? 

		       			<Nav.Link href="/logout" className="text-light">Logout</Nav.Link>

		       		: 
		       		<Fragment>
		       			   	<Nav.Link href="/login" className="text-light">Login</Nav.Link>
		       				<Nav.Link href="/register" className="text-light">Register</Nav.Link>
		       		</Fragment>
		       	}
		    </Nav>
		    <Form inline>
		      <FormControl type="text" placeholder="Search" className="mr-sm-2" />
		      <Button variant="outline-success">Search</Button>
		    </Form>
		  </Navbar.Collapse>
		</Navbar>
	)

}

