import {Fragment} from 'react';
import {Link} from 'react-router-dom';

// export default function Banner(indicator){

// 	//console.log(indicator);
// 	let greeting;
// 	let text;

// 	if (indicator.indicator == null){
// 		greeting = `Welcome to Course Booking App!`;
// 		text = `Opportunities for everyone, everywhere`;
// 	} else {
// 		greeting = `Zuitt Booking`;
// 		text = `Page Not Found`;
// 	} 

// 	return(
// 		<div className="jumbotron jumbotron-fluid">
// 	  		<div className="container">
// 	    		<h1 className="display-4">{greeting}</h1>
// 	    		<p className="lead">{text}</p>
// 	    		{
// 	    			indicator.indicator == null ?
// 	    			<a className="btn btn-info" href="#">Click here</a>
// 	    			: <p>Go back to the <Link to="/">homepage</Link></p>
// 	    		}
	  			
// 	  		</div>
// 		</div>
// 	)

// }

export default function Banner({bannerProp}){
	//destructured because object received will be object
	/*
	const {bannerProp} = props
	const props = {
		bannerProp: {
			title: "Error 404",
			description: "Page not found",
			destination: "/",
			buttonDesc: "Go back home"
		}
	}

	*/

	//console.log(bannerProp);
	const {title, description, destination, buttonDesc} = bannerProp;

	return(
		<div className="jumbotron jumbotron-fluid">
	  		<div className="container">
	    		<h1 className="display-4">{title}</h1>
	    		<p className="lead">{description}</p>
	    		<a className="btn btn-info" href={destination}>{buttonDesc}</a>
	  		</div>
		</div>
	)

}