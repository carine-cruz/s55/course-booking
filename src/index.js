import React, {Fragment} from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';

import App from './App';
// import reportWebVitals from './reportWebVitals';

ReactDOM.render(
  //if not destructured, <React.Fragment>
  <Fragment>
    <App />
  </Fragment>
, document.getElementById('root')

);